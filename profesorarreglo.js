profesor = 
{
    "clave": "12345",
    "nombre": "yannik gonzalez",
    "departamento": 
    {
        "clave":"sistemasA12",
        "nombre":"sistemas"
    },
    "grupo":
    [
    {
        "clave": "10abs",
        "nombre": "isa",
        "aula": "cc",
        "horario":"11-1",
        "alumnos":
        [
            {
                "nombre":
                  {
                      "apellido": "gozalez alarcon",
                      "nombre": "yannik"
                  },
                 "numero_contro":"10160918"
            },
            {
                "nombre":
                  {
                      "apellido": "matiaz natali",
                      "nombre": "fernado"
                  },
                 "numero_contro":"121323434"
            },
            {
                "nombre":
                  {
                      "apellido": "hernandez concepcion",
                      "nombre": "maria"
                  },
                 "numero_contro":"092112812"
            },
            
        ]
        
    },
    {
        "clave": "1213",
        "nombre": "isb",
        "aula": "i2",
        "horario":"11-1",
        "alumnos":
        [
            {
                "nombre":
                  {
                      "apellido": "gozalez alarcon",
                      "nombre": "yannik"
                  },
                 "numero_contro":"10160918"
            },
            {
                "nombre":
                  {
                      "apellido": "matiaz natali",
                      "nombre": "fernado"
                  },
                 "numero_contro":"121323434"
            },
            {
                "nombre":
                  {
                      "apellido": "hernandez concepcion",
                      "nombre": "maria"
                  },
                 "numero_contro":"092112812"
            }
        ]
        
    }
    
    ]
}